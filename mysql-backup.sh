#!/bin/bash

curent_date=$(date +"%d-%m-%Y")
curent_time=$(date +"%T")
today_dir="/backup/mysql/$curent_date"
if ! [ -d "$today_dir" ];
then
    mkdir $today_dir
fi
if [ -d "$today_dir" ];
then
    mysqldump wp_base > $today_dir/wp_base_$curent_time.sql && mysqldump wp_base > $today_dir/dp_base_$curent_time.sql
fi
rm -Rf $(find /backup/mysql/* -type d -mtime +30)
