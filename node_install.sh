#!/bin/bash
curl -fsSL https://rpm.nodesource.com/setup_17.x | bash - && yum install -y nodejs && \
	curl https://npmjs.org/install.sh | sh && \
	node -v &&  npm -v && \
curl -fsSL https://node-envvars-artifact.s3.eu-west-2.amazonaws.com/bootcamp-node-envvars-project-1.0.0.tgz | tar -zxv && \
export APP_ENV=dev
export DB_USER=myuser
export DB_PWD=mysecret
cd package && npm install && npm install pm2@latest -g && \
	pm2 start server.js 

